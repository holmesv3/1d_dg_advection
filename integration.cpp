#include "integration.h"

double trapezoidal_rule(func& f, double a, double b, int n) {
	
	double width = b - a;
	double step = width / n;
	double result = 0;

	for (int i = 0; i < n - 1; i++)
		result += step / 2. * (f(a + step * i) + f(a + step * (i + 1)));
	return result;

}
double simpson_one_third(func& f, double a, double b, int n) {
	
	double width = b - a;
	double step = width / (n*2);
	double result = 0;

	for (int i = 0; i < n; i++)
		result += step / 3. * (f(a + step*(2*i - 2)) + 4*f(a + step*(2*i - 1)) + f(a + step*(2*i)) );
	return result;
}

double adaptive_quadrature(func& f, double a, double b, double t ) {
	
	double r1, r2, diff;
	int n = 1;
	while (1) {
		r1 = simpson_one_third(f, a, b, n);
		r2 = simpson_one_third(f, a, b, n + 1);
		diff = abs(r2 - r1);

		if (diff <= t)
			return r2 + (r2 - r1) / 15;

		if (n > 100)
			return r2 + (r2 - r1) / 15;

		n++;
	}
	return 0;
}

double gauss_lobatto(func& f, double a, double b, int n) {
	double result = 0;
	std::vector<double> nodes, weights;
	get_lgl_data(n - 1, weights, nodes, a, b);
	for (int i = 0; i < n; i++)
		result += weights[i] * f(nodes[i]);
	return result;
}
