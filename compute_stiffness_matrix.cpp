#include "compute_stiffness_matrix.h"

void compute_stiffness_matrix(double a, double b, dbl_mat& stiff_matrix, func_vec lag_poly) {
	
	func df, f;
	int m = lag_poly.size();
	int n = m;
		if (n == 2) n = 3;

	double step = (b-a)/(n);
	// Compute stiffness matrix
	for (int i = 0; i < m; i++)
		for (int j = 0; j < m; j++) {
			df = std::bind(func_deriv, std::placeholders::_1, step, lag_poly[i]);
			f = std::bind(func_mult, std::placeholders::_1, lag_poly[j], df);
			stiff_matrix[i][j] = (b-a)*gauss_lobatto(f, a, b, n)/2;
		}
}



