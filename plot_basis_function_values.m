function plot_basis_function_values()
    close all; 
    cd(fileparts(which(mfilename)))
    
    myfile = "function_values.txt";
    
    file = fopen(myfile);
  
    data = fscanf(file, '%f \t %f');
    elem = data(1);
    nx = data(2);
    nf = data(3);
    
    x = data(4:nx+3);
    f = zeros(nf, nx);
    for i = 1:nf
        f(i, :) = data(4+nx*i:4+nx*i+nx-1);
    end
    figure
    hold on
    for i = 1:nf
        plot(x, f(i, :), 'linewidth', 2)
    end
    for i = 1:nf
        legend_cells{i} = sprintf('%s %d %s', "\phi_{", i, "}(x)");
    end
    legend(legend_cells)
    axis([x(1) x(end) -inf inf])
    title_string = sprintf('%s %d %s %d',"Basis functions for element ", elem+1, "; nx = ", nx);
    title(title_string);
    xlabel('x');
    ylabel('\phi_n(x)');

end