#include <vector>
#include <functional>
#include "element_class.h"

#ifndef __FLUX_MATRIX__
#define __FLUX_MATRIX__
typedef std::vector<double> dbl_vec;
typedef std::vector<std::vector<double>> dbl_mat;
typedef std::function<double(double)> func;
typedef std::vector<func> func_vec;
typedef std::vector<ELEMENT*> elem_vec;

void upwind_flux(elem_vec elements);
void downwind_flux(elem_vec elements);

#endif