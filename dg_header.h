#pragma once
#ifndef __DG_HEADER__
#define __DG_HEADER__

// Standard Libraries
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <vector>

// Custom "mathy" libraries
#include "dg_math.h"
// Custom objects
// The main program only sees the grid. All other levels of organization are hidden

#endif