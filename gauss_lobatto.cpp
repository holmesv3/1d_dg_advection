#include "gauss_lobatto.h"

double gauss_lobatto(int n, std::vector<double> &weights, 
	std::vector<double> &f, bool include_remainder) {

	double result = 2/(n*(n-1)) * (f.front() + f.back()); // 1st order integration term
	
	for (int i = 1; i < n - 1; i++)
		result += weights[i] * f[i];

	if (include_remainder) {
		return result + (-n*pow((n-1.), 3.)*(pow(2, 2*n-1))*(pow(fact(n-2), 4))) /
			((2*n - 1)*(pow(fact(2*n - 2), 3))) * f[1];
	}
		return result;
}

int fact(int x) {
	unsigned long int factorial = 1;
	for (int i = 1; i < x; i++)
		factorial *= i;

	return factorial;
}