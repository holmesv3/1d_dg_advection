#include "grid_class.h"

// Constructor
GRID::GRID( const dbl_vec &x, const dbl_vec &dx, int nx, int solution_order)
{
	/* The GRID constructor recieves
		simulation space pointer (*x)
		dx pointer (*dx)
		size of the space (nx)
		and order of solution (solution_order)
	*/

	// Messages showing where parameters are being read from
	std::cout << "\nParameters being read from : \n";
	std::cout << "\tWithin program \n";

	// Initializing GRID member variables
	number_of_elements = nx - 1;
	grid_solution_order = solution_order;

	x_min = x.front();
	x_max = x.back();

	grid_coordinates = x;
	grid_dx = dx;

	// Display parameters
	std::cout << "Simulation parameters :  \n";
	std::cout << "\tXmin \t= " << x_min << "\n";
	std::cout << "\tXmax \t= " << x_max << "\n";

	// Assuming linear spacing for now
	//std::cout << "\tdX \t= " << *grid_dx << "\n";
	std::cout << "\tSolution order \t= " << grid_solution_order << "\n";

	/* Initialzes ELEMENT objects*/
	std::cout << " \n";
	std::cout << "Initialzing " << number_of_elements << " elements\n";
	std::cout << " \n";

	for (int i = 0; i < number_of_elements; i++) {
		// Each element recives the solution order and its boundaries.
		ELEM.push_back(new ELEMENT(grid_solution_order, grid_coordinates[i], grid_coordinates[i + 1]));
	}
	
	// Assign upwind flux scheme to elements
	upwind_flux(ELEM);

	// "Connect" neighboring elements together
	link_elements(ELEM);
}

GRID::~GRID() {

	for (int i = 0; i < number_of_elements; i++) 
		delete ELEM[i];
	
	ELEM.clear();
}

void GRID::print_grid_coordinates() {
	std::cout << "Solution grid coordinates : \n";
	for (auto i = grid_coordinates.begin(); i != grid_coordinates.end(); i++) {
		std::cout << *i << '\t';
	}
	std::cout << std::endl;
}

void GRID::print_element_node_coordinates(int m) {
	if (m == -1) {
		std::cout << "Coordinates of all elements " << std::endl;
		for (int i = 0; i < number_of_elements; i++) {
			std::cout << "Element " << i + 1 << std::endl;
			ELEM[i]->print_node_coordinates();
		}
		return;
	}
	std::cout << "Coordinates of ELEMENT " << m + 1 << std::endl;
	ELEM[m]->print_node_coordinates();
}

void GRID::print_element_data(int m) {
	std::cout << "Data for element " << m + 1 << '\n';
	ELEM[m]->print_element_data();
}

void GRID::output_initial_condition(IC_func& f,std::string outfile) {
		std::cout << "Outputting all node values to " << outfile << '\n';
		std::ofstream out_handle;
		out_handle.open(outfile);
		out_handle << number_of_elements << '\n';
		out_handle << grid_solution_order << '\n';
		for (int i = 0; i < number_of_elements; i++)
			ELEM[i]->print_node_coordinates(out_handle);
		for (int i = 0; i < number_of_elements; i++)
			ELEM[i]->print_node_values(out_handle);
		out_handle << "##########";
		out_handle.close();
		analytic_solution(0, f);
	
}
void GRID::output_solution_approximation(double t, IC_func& f, std::string outfile) {
	std::cout << "Outputting all node values to " << outfile << '\n';
	std::ofstream out_handle;
	out_handle.open(outfile);
	out_handle << number_of_elements << '\n';
	out_handle << grid_solution_order << '\n';
	for (int i = 0; i < number_of_elements; i++)
		ELEM[i]->print_node_coordinates(out_handle);
	for (int i = 0; i < number_of_elements; i++)
		ELEM[i]->print_node_values(out_handle);
	out_handle << "##########";
	out_handle.close();
	analytic_solution(t, f);

}
void GRID::output_element_functions(int m, int n, std::string file) {
	std::ofstream out_handle;

	if (file == "NULL") {
		std::cout << "\nOutputting all function values to function_values.txt" << '\n';
		out_handle.open("function_values.txt"); // Default file read by matlab script
	}
	else {
		std::cout << "Outputting all function values to " << file << '\n';
		out_handle.open(file);
	}
	out_handle << m << '\n'; // Element number
	out_handle << n << '\n'; // Give the number of points for processing
	out_handle << ELEM[m]->number_of_basis << '\n'; // Number of functions
	for (double i = 0; i < n; i++) // Coordinates
		out_handle << grid_coordinates[m] + grid_dx[m] * (i) / (n-1) << '\n';
	for (int i = 0; i < ELEM[m]->number_of_basis; i++)
		ELEM[m]->output_function_values(i, n, out_handle);
	out_handle.close();
}

void GRID::give_initial_condition(IC_func& f) {
	// Passes to element functions
	for (int i = 0; i < number_of_elements; i++)
		ELEM[i]->get_initial_condition(f);
}

void GRID::call_elements_time_step(double dt, double& real_t, double& sim_t, int n) {
	std::cout << "Performing  " << n << "  time steps of  "<< dt <<"  seconds. . . . .";
	std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
	start = std::chrono::high_resolution_clock::now();
	//pass_element_boundary_values(ELEM);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < number_of_elements; j++) {
			ELEM[j]->perform_time_step(dt);
		}
		for (int j = 0; j < number_of_elements; j++) {
			ELEM[j]->update_boundaries();
		}
	}
	end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed_time = end - start;
	real_t += elapsed_time.count();
	sim_t = +dt * n;
	std::cout << " Completed. \n\tTime elapsed =  " << elapsed_time.count() << " seconds. Total elapsed time = "<< real_t << " seconds\n";
}

void GRID::analytic_solution(double t, IC_func& f, std::string outfile) {
	std::ofstream analytic;
	if (outfile == "NULL") {
		if (t == 0)
			analytic.open("analytic_initial_condition.txt");
		else
			analytic.open("analytic_solution.txt");
	}
	for (int i = 0; i < number_of_elements; i++)
		ELEM[i]->analytic_solution(t, f, analytic);

	analytic.close();
}

void GRID::link_elements(std::vector<ELEMENT*> ELEMS) {
	// Periodic boundary
	link_elements(ELEMS.back(), ELEMS[0]);
	for (int i = 0; i < ELEMS.size() - 1; i++)
		link_elements(ELEMS[i], ELEMS[i + 1]);
}
void GRID::link_elements(ELEMENT* ELEM1, ELEMENT* ELEM2) {
	double dummy = 0;
	// "Link" element 2 to the element to its left [E1][E2]
	// This make the right side of E1 look into E2, and vice versa
	ELEM2->LEFT_SIDE.value = &ELEM1->right_value;
	ELEM2->LEFT_SIDE.flux  = -ELEM1->flux_vec.back();
	

	// Repeat for the other direction
	dummy = 0;
	ELEM1->RIGHT_SIDE.value = &ELEM2->left_value;
	ELEM1->RIGHT_SIDE.flux = -ELEM2->flux_vec.front();
	
}