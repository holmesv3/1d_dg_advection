#include "lagrange.h"

double lagrange_term(double x, double xm, double xi) {
	return (x - xi) / (xm - xi);
}
func lagrange_poly(int m, dbl_vec& nodes) {
	func result;
	func_vec lag_terms;
	
	for (int i = 0; i < m; i++) 
		lag_terms.push_back(std::bind(lagrange_term, std::placeholders::_1 , nodes[m], nodes[i]));
	for (int i = m+1; i < nodes.size(); i++)
		lag_terms.push_back(std::bind(lagrange_term, std::placeholders::_1, nodes[m], nodes[i]));
	
	result = std::bind(lagrange_product, std::placeholders::_1, lag_terms);
	return result;

}
double lagrange_product(double x, func_vec& f) {
	double result = f[0](x); // 0th order term
	for (int i = 1; i < f.size(); i++)
		result *= f[i](x);

	return result;
}


double func_mult(double x, func& f1, func& f2) {
	return f1(x) * f2(x);
}
double func_deriv(double x, double step, func& f) {
	double upper = x + step / 2.;
	double lower = x - step / 2.;
	return (f(upper) - f(lower)) / step;
}

