#include <vector>
#include <functional>
#include "legendre.h"

#ifndef __DG_INTEGRATION__
#define __DG_INTEGRATION__
typedef std::function<double(double)> func;
double trapezoidal_rule(func& f, double lower_limit = 0., double upper_limit = 1., int number_of_points = 2);
double simpson_one_third(func& f, double lower_limit = 0., double upper_limit = 1., int number_of_points = 2);
double adaptive_quadrature(func& f, double lower_lim = 0., double upper_lim = 1., double tolerance = 0.1);
double gauss_lobatto(func& f, double lower_lim = 0., double upper_lim = 1., int number_of_points = 10);

#endif

