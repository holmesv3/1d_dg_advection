#include <vector>
#include <functional>
#include "integration.h"
#include "lagrange.h"

#ifndef __STIFFNESS_MATRIX__
#define __STIFFNESS_MATRIX__

void compute_stiffness_matrix(double left_bound, double right_bound, dbl_mat& stiff_matrix, func_vec lagrange_polynomials);

#endif