#include "compute_mass_matrix.h"

void compute_mass_matrix(double a, double b, dbl_mat& mass_matrix, func_vec& lag_poly, dbl_vec weights) {

	func f = lag_poly[0];
	int m = lag_poly.size();
	int n = m;
	if (n == 2) n = 3;
	// Compute mass matrix
	for (int i = 0; i < m; i++)
		for (int j = 0; j < m; j++) {
			f = std::bind(func_mult, std::placeholders::_1, lag_poly[j], lag_poly[i]);
			mass_matrix[i][j] = (b-a)*gauss_lobatto(f, a, b, n);
			}
}

