#pragma once
#include <vector>
#ifndef __DERIVATIVES__
#define __DERIVATIVES__

void forward_derivative(int n, std::vector<double> &array, std::vector<double> &dx, std::vector<double> &result);
void backward_derivative(int n, std::vector<double> &array, std::vector<double> &dx, std::vector<double> &result);

#endif