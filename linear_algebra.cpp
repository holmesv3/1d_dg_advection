#include "linear_algebra.h"

void decompose(dbl_mat& a, int_vec& o, dbl_vec& s, int n){
	for (int i = 0; i < n; i++) {
		o.push_back(i);
		s.push_back(abs(a[i][1]));
		for (int j = 1; j < n; j++)
			if (abs(a[i][j]) > s[i])
				s[i] = abs(a[i][j]);
	}
	double factor;
	for (int j = 0; j < n - 1; j++) {
		pivot(a, o, s, n, j);
		for (int i = j + 1; i < n; i++) {
			factor = a[o[i]][j] / a[o[j]][j];
			a[o[i]][j] = factor;
			for (int k = j + 1; k < n; k++)
				a[o[i]][k] = a[o[i]][k] - factor * a[o[j]][k];
		}
	}
}

void substitue(dbl_mat& A, dbl_vec& B, dbl_vec& result, int_vec& o, int n){
	// Forward substitution
    int m = n - 1;
	double temp;
	for (int i = 1; i < n; i++) {
		temp = B[o[i]];
		for (int j = 0; j < i; j++)
			temp = temp - A[o[i]][j] * B[o[j]];
		B[i] = temp;
	}
	// Backward substitution
	result.back() = B[o[m]] / A[o[m]].back();
	for (int i = m - 1; i >= 0; i--) {
		temp = 0.;
		for (int j = i + 1; j < n; j++)
			temp = temp + A[o[i]][j] * result[j];
		result[i] = (B[o[i]] - temp) / A[o[i]][i];
	}
}

void pivot(dbl_mat& a, int_vec& o, dbl_vec& s, int n, int k) {
	int p = k;
	double big = abs(a[o[k]][k] / s[o[k]]);
	double dummy;
	for (int i = k + 1; i < n; i++) {
		dummy = abs(a[o[i]][k] / s[o[i]]);
		if (dummy > big) {
			big = dummy; 
			p = i;
		}
	}
	dummy = o[p];
	o[p] = o[k];
	o[k] = dummy;
}

void LU_decomposition(dbl_mat& A, dbl_vec& B, dbl_vec& result){
	int n = B.size();
	dbl_vec s;
	int_vec o;
	decompose(A, o, s, n);
	substitue(A, B, result, o, n);
}

double scalar_vector_multiplication(double a, dbl_vec& b) {
	double result = 0;
	int n = b.size();
	for (int i = 0; i < n; i++) 
		result += a * b[i];
	return result;
}

dbl_vec vector_matrix_multiplication(dbl_vec& a, dbl_mat& b) {
	int n = a.size();
	dbl_vec result;
	for (int i = 0; i < n; i++) {
		result.push_back(scalar_vector_multiplication(a[i], b[i]));
	}
	return result;
}

void matrix_inverse(dbl_mat& A, dbl_mat& result){
	int n = A[0].size();
	dbl_vec B;
	dbl_mat dummy;
	dbl_vec temp;
	dbl_vec s;
	int_vec o;

	// Initialize computation variables
	for (int i = 0; i < n; i++) {
		B.push_back(0.);
		temp.push_back(0.);
	}
	for(int i = 0; i < n; i++)
		dummy.push_back(temp);

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			result[i][j] = A[i][j];
			dummy[i][j] = A[i][j];
		}

	}
	// Begin inverse computation
	decompose(dummy, o, s, n);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (i == j)
				B[j] = 1.;
			else
				B[j] = 0.;
		}
		substitue(dummy, B, temp, o, n);
		for (int j = 0; j < n; j++)
			result[j][i] = temp[j];
	}
}
