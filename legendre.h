#include <cmath>
#include <vector>
#include <functional>
#include <algorithm>

#ifndef __LGL_HEADER__
#define __LGL_HEADER__

typedef std::vector<double> dbl_vec;
typedef std::vector<std::vector<double>> dbl_mat;

// Stores the legendre nodes and weights for a given accuracy in RESULTS_PTR
void get_lgl_nodes_weights(int solution_order, dbl_vec &result_ptr);

// Stores the value of the legendre nodes scaled to [left, right] in NODES_PTR and weights in WEIGHTS_PTR
void get_lgl_data(int solution_order, dbl_vec&weights_ptr, dbl_vec& nodes_ptr, double left = 0., double right = 1.);

#endif 