#include <vector>
#include <functional>

#ifndef __LAGRANGE__
#define __LAGRANGE__

typedef std::function<double(double)> func;
typedef std::vector<double> dbl_vec;
typedef std::vector<std::function<double(double)>> func_vec;


// Stencil for the (x - xi)/(xm-xi) terms of the Lagrange Polynomials
double lagrange_term(double x, double xm, double xi);
// Multiplies the individual terms together to form the Lagrange Polynomial
double lagrange_product(double x, func_vec& f);
// Creates the m'th order Lagrange Polynomail
func   lagrange_poly(int m, dbl_vec& nodes);

double func_mult(double x, func& f1, func& f2);
double func_deriv(double x, double step, func& f);

#endif