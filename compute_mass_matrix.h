#include "lagrange.h"
#include "integration.h"

#include <vector>
#include <functional>
#ifndef __MASS_MATRIX__
#define __MASS_MATRIX__
void compute_mass_matrix(double left_bound, double right_bound, dbl_mat& mass_matrix, func_vec& basis_polynomials, dbl_vec legendre_weights);

#endif
