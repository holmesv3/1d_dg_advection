#include "legendre.h"

/*
Legendre nodes/weights computation adapted from MATLAB script written by Greg von Winckel "lglnodes.m"

https://www.mathworks.com/matlabcentral/fileexchange/4775-legende-gauss-lobatto-nodes-and-weights

*/

void get_lgl_nodes_weights(int solution_order, dbl_vec& ptr_to_return) {
	
	// Compute pi
	double PI = 2 * acos(0);

	// Number of nodes and weights
	int m = solution_order + 1; 

	// Declaring computation variables
	dbl_vec lgl_nodes_ptr;   // Current node coordinates
	dbl_vec old_lgl_nodes;   // Past iteration node coordinates
	dbl_mat LV_matrix;	     // Legendre Vandermonde matrix
			
	// Exit conditon variables
	std::vector<double> array_difference ;  //   "		"        "
	
	
	// Initialze nodes to the Chebyshev-Gauss-Lobatto nodes; old_nodes vector and array_difference to 0's
	for (int i = 0; i < m; i++) {
		lgl_nodes_ptr.push_back(cos(PI * i / solution_order));
		old_lgl_nodes.push_back(0);
		array_difference.push_back(0);
	}

	// Initialze LV matrix to 0's
	for (int i = 0; i < m; i++)
		for (int j = 0; j < m; j++)
			LV_matrix.push_back(old_lgl_nodes);


	// While loop exit condition
	double precision = pow(2, -52); // = eps (from .m file)
	double max_error = 100;

	while (max_error >= precision) {

		// Update first two rows of LV matrix and the old node values
		for (int i = 0; i < m; i++) {
			old_lgl_nodes[i] = lgl_nodes_ptr[i];
			LV_matrix[i*m][0] = 1; // P(:, 1) = 1;
			LV_matrix[i*m][1] = lgl_nodes_ptr[i]; // P(:, 2) = x;
		}

		// Compute other rows of LV matrix
		for (int i = 0; i < m; i++) {
			for (long int j = 1; j < m - 1; j++) {		  
				LV_matrix[i*m][j+1] = (lgl_nodes_ptr[i] * (2.*(j+1.) - 1)*LV_matrix[i*m][j] - LV_matrix[i*m][(j-1)] * j) / ((double)j + 1.);
			}
		}

		// Recompute the value of the LGL nodes
		for (int i = 0; i < m-1; i++)
			lgl_nodes_ptr[i] = old_lgl_nodes[i] - (lgl_nodes_ptr[i] *
				LV_matrix[i*m][m-1] - LV_matrix[i* m][m-2]) /
			(LV_matrix[i*m][m-1] * m);


		// Update loop exit condition
		for (int i = 0; i < m-1; i++)
			array_difference[i] = lgl_nodes_ptr[i] - old_lgl_nodes[i];
		max_error = abs(*(std::max_element(array_difference.begin(), array_difference.end())));

	}
	// Once complete compute weights, passing both values to ptr (n0, w0, ... nN, wN)
	for (int i = 0; i < m; i++) {
		ptr_to_return.push_back(lgl_nodes_ptr[(m-1) - i] );
		ptr_to_return.push_back( 2. / ((m - 1.) * m * pow(LV_matrix[i * m][(m - 1)], 2.)));
	}
	
}

void get_lgl_data(const int solution_order, dbl_vec& weights_ptr, dbl_vec& nodes_ptr, double left, double right) {
	// Returns the legendre nodes scaled between left_bound and right_bound
	// Shifts nodes from [-1 1] to [0 1], multiples by the desired dX, and shifts left or right accordingly. defaults to [0 1]
	// Shift is measured from the left node, defaults to 0
	double shift = left;
	double dx = right - left;
	double shifted_node;

	dbl_vec result;
	get_lgl_nodes_weights(solution_order, result);
	for (int i = 0; i <= solution_order; i++) {
		shifted_node = (((result[2*i] + 1.) / 2.) * dx) + shift;
		nodes_ptr.push_back(shifted_node);
		weights_ptr.push_back(result[2*i + 1]);
	}
}


