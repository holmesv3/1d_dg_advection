#include "legendre.h"    // Math for LGL nodes and weights
#include "lagrange.h"	 // Math for Lagrange polynomials

#include "neighbor_struct.h"

#include "compute_mass_matrix.h"
#include "compute_stiffness_matrix.h"
#include "linear_algebra.h"

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <functional>
#include <fstream>

#ifndef __ELEMENT_CLASS_H__
#define __ELEMENT_CLASS_H__

class ELEMENT {
	typedef std::function<double(double, double)> IC_func;

public:
	
	// Constructor, initializes basis functions
	ELEMENT(int grid_solution_order, double left_node, double right_node); 
	// Prints node coordinates to terminal
	void print_node_coordinates();	
	// Prints node coordinates to the file pointed to by the handle
	void print_node_coordinates(std::ofstream& handle);
	// Outputs the values of the m'th basis function across the element with n points to the handle file
	void output_function_values(int m, double n, std::ofstream& handle);
	// Prints the mass, mass inverse, stiffness, and flux values to the terminal
	void print_element_data();
	// Prints the element node values to the terminal
	void print_node_values();
	// Prints the element node values to the handle
	void print_node_values(std::ofstream& handle);
	// Computes the stencil matrix for the element
	void compute_stencil(); 

	void get_initial_condition(IC_func& initial_condition); // Calls the stencil creation routine as well
	void perform_time_step(double dt);
	void update_boundaries();

	void analytic_solution(double t, IC_func& f, std::ofstream& handle);
	
	int number_of_nodes;	 // Number of points within the element
	int element_solution_order;  // Solution accuracy
	int number_of_basis;		 // Number of basis functions

	double left_bound;			 // Left boundary of element
	double right_bound;			 // Right  "      "     "

	double left_value;
	double right_value;

	// Variables to link the elements together
	NEIGHBOR LEFT_SIDE;
	NEIGHBOR RIGHT_SIDE;
	
	double c;

	dbl_vec node_coordinates;	 // Coordinates of the points within each element
	dbl_vec node_values;		 // Values at each point within the element
	dbl_vec lgl_weights;		 // Weights of the legendre nodes for integration
	dbl_vec lgl_nodes;			 // Values of the legendre polynomials for each element. N'th basis in N'th row

	dbl_vec flux_vec;

	dbl_vec stencil_mass;
	dbl_vec stencil_stiff_flux;

	dbl_mat mass_matrix;
	dbl_mat mass_matrix_inverse;
	dbl_mat stiffness_matrix;


	func_vec element_lagrange_polynomials; // Vector containing Lagrange polynomials
};

#endif
