#include <vector>
#include <cmath>

#ifndef __LINEAR_ALGEBRA__
#define __LINEAR_ALGEBRA__

typedef std::vector<double> dbl_vec;
typedef std::vector<int> int_vec;
typedef std::vector<dbl_vec> dbl_mat;

void decompose(dbl_mat& A, int_vec& o, dbl_vec& s,  int n);

void substitue(dbl_mat& A, dbl_vec& B, dbl_vec& result, int_vec& o, int n);

void pivot(dbl_mat& A, int_vec& o, dbl_vec& s, int n, int k);

void LU_decomposition(dbl_mat& A, dbl_vec& B, dbl_vec& result);

double scalar_vector_multiplication(double A, dbl_vec& B);
dbl_vec vector_matrix_multiplication(dbl_vec& A, dbl_mat& B);

void matrix_inverse(dbl_mat& A, dbl_mat& result);
#endif