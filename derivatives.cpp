
#include "derivatives.h"
/*
    Backward and Forward derivative functions:
        Inputs :
        n           =   dimension of array to operate on
        array[n]    =   pointer to first element of operand -
        dx[n-1]     =   spacing between the (i)th and (i+1)th element
        result[n-1] =   stored derivative results for the array
*/

void backward_derivative(int n, std::vector<double> &array, std::vector<double> &dx, std::vector<double> &result)
{
    for (int i = 1; i < n; i++) {
        result.push_back((array[i] - array[i - 1]) / dx[i - 1]);
    }
}

void forward_derivative(int n, std::vector<double> &array, std::vector<double> &dx, std::vector<double> &result)
{
    for (int i = 0; i < n - 1; i++) {
        result.push_back((array[i + 1] - array[i]) / dx[i]);
    }
}