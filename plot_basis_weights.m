
function plot_basis_weights()
    close all;
    
    cd(fileparts(which(mfilename)))
    myfile = "basis_weights.txt";
    file = fopen(myfile);
    basis_weights = fscanf(file, '%f');
    elements = basis_weights(1);
    order = basis_weights(2);
    num_nodes = order+1;
    
    x = basis_weights(3:elements*num_nodes+2);
    basis_weights = basis_weights(elements*num_nodes+3:end);
    elem_weights = zeros(elements, num_nodes);
    for i = 0:elements-1
        elem_weights(i+1, :) = basis_weights(i*num_nodes + 1:i*num_nodes + num_nodes);
    end
    
    figure
    hold on
    for i = 1:elements
        plot(x((i-1)*num_nodes+1:(i-1)*num_nodes + num_nodes), elem_weights(i, :), 'linewidth', 2);
    end
    axis([x(1) x(end) -1 1])
    title("Basis weights")
    
    % Analytic solution
    soln_file = "analytic_solution.txt";
    file = fopen(soln_file);
    analytic_soln = fscanf(file, '%f');
    plot(x, analytic_soln, '--k','linewidth', 1.5)

  
end