#include <vector>
#include <cmath>
#ifndef __GAUSS_LOBATTO_INTEGRATION__
#define __GAUSS_LOBATTO_INTEGRATION__

/* Routine for performing Gauss-Lobatto quadrature across some range of values. 

	THE "FUNCTION" PASSED IS THE FUNCTION ALREADY COMPUTED AT THE NODES

*/

double gauss_lobatto(int number_of_points, std::vector<double>& weights,
	std::vector<double>& function, bool include_remainder = 0);

int fact(int x);

#endif
