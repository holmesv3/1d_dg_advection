
#include "grid_class.h"
#include <iostream>
#include <functional>
#include <vector>
#include <cmath>

void initialize_space(dbl_vec &x_space,dbl_vec &dx, int nx) {
	/* Used to manually initialze the simulation space
	*   *x_space = pointer to the X coordinates [nx]
	*   *dx = pointer to the dx values (from left to right) [nx-1]
	*    nx = x_space dimension
	  */
	for (double i = 0; i < nx; i++)
		x_space.push_back((i / (nx - 1.)));

	for (double i = 0; i < nx - 1; i++)
		dx.push_back(1. / (nx-1));
}

double INITIAL_CONDITION(double x, double t) {
	double pi = 2.*acos(0.);
	//return 0 + t;
	return sin(x*2*pi - t);
	
}

int main()
{
	const int MAX_ORDER = 100; // Up to 100th order

	/* In the current state of things, main() initializes the simulation space, then creates a GRID object.

	*/
 //_____________________________________________
 // PARAMETERS ---------------------------------|
 // --------------------------------------------|__
	int N = 4;			    // Number of elements  |
    int SOLUTION_ORDER = 1; // Solution accuracy   |
 // -----------------------------------------------|
 // ______________________________________________/

	int nx = N + 1;                // Dummy variables until I get a param file
	dbl_vec x_space;   // Only used to intialze GRID object.
	dbl_vec dx;        //

	// -------------------

	if (SOLUTION_ORDER > MAX_ORDER) {
		std::cerr << "WARNING - Maximum solution order = " << MAX_ORDER << '\n';
		std::cerr << "Order will default to 1 \n";
		SOLUTION_ORDER = 1;
	}
	if (N < 2) {
		std::cerr << "WARNING - Can't simulate 1 element" << MAX_ORDER << '\n';
		std::cerr << "Number of elements will default to 2 \n";
		N = 2;
	}
	
	// Initialze dummy space parameters
	initialize_space(x_space, dx, nx);
	
	// Initialize solution grid
	GRID DG(x_space, dx, nx, SOLUTION_ORDER);

	// Outputes the values of all element m's functions 
	// with n (default 50) points across the element to the file "function_values.txt"
	//	DG.output_element_functions(0, 500);

	// Prints matrices and flux vector for element m default 0
		 DG.print_element_data();

	// Passes the initial condition as a function of f(x, t)
	std::function<double(double, double)> f = INITIAL_CONDITION;
	DG.give_initial_condition(f);
	// Supply a string to output to the file, otherwise, prints to terminal
	DG.output_initial_condition(f); 

	// ==================== BEGIN TIME STEPPING
	
	double real_t = 0; // Keeps track of the total time spent in the simulation
	double sim_t = 0;  // Keeps track of the simulation time for analytic calculation
	// Tells the elements to step forward by dt, n (default 10) times
	DG.call_elements_time_step(0.000001, real_t, sim_t, 100000);
	DG.output_solution_approximation(sim_t, f); // Read by matlab script
	
	// Exit program with code 0, calls ~GRID;
	return 0;
}