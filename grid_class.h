#include "element_class.h"
#include "numerical_flux.h"

#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <chrono>

#ifndef __GRID__
#define __GRID__

class GRID {
	typedef std::function<double(double, double)> IC_func;
public:

	// Constructor
	// Initializes member variables and ELEMENTS
	GRID(const dbl_vec &x, const dbl_vec &dx, int nx, int solution_order); 
	// Destructor
	~GRID(); 
            
	 // Displays the grid coordinates
	void print_grid_coordinates();     
	// Calls the m'th element's node coordinates, or all by default
	void print_element_node_coordinates(int m = -1);
	// Gets the M, K, and F_hat info for element n
	void print_element_data(int m = 0);
	// Outputs the initial basis weights to a txt file and the analytic IC
	void output_initial_condition(IC_func& f, std::string outfile = "initial_basis_weights.txt");
	// Outputs the current basis weights at time t as well as the analytic solution
	void output_solution_approximation(double t, IC_func& f, std::string outfile = "basis_weights.txt");
	// Prints the value of all element functions to a file
	void output_element_functions(int m, int n = 50, std::string file = "NULL");
	// Passes the initial condition to each element
	void give_initial_condition(IC_func& f);
	// Performs n (default 10) time steps of dt and keeps track of total time
	void call_elements_time_step(double dt, double& real_time, double& sim_time, int n = 10);
	// Outputs the analytic solution
	void analytic_solution(double t, IC_func& f, std::string outfile = "NULL");
	// Initializes the pointers within each element to the proper values;
	void link_elements(std::vector<ELEMENT*> ELEM_vec);
	// Points the upwind element to the downwind
	void link_elements(ELEMENT* left, ELEMENT* right);




private:

	int grid_solution_order;    // Solution accuracy
	int number_of_elements;     // Number of ELEMENT objects

	double x_min;               // Left boundary of domain
	double x_max;               // Right  "      "     "

	dbl_vec grid_coordinates;   // Coordinates of the grid nodes
	dbl_vec grid_dx;            // Distance between grid nodes, measured from the left
	
	std::vector<ELEMENT*> ELEM;
};

#endif