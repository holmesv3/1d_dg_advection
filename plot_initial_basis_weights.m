
function plot_initial_basis_weights()
    close all;
    
    cd(fileparts(which(mfilename)))
    myfile = "initial_basis_weights.txt";
    file = fopen(myfile);
    basis_weights = fscanf(file, '%f');
    elements = basis_weights(1);
    order = basis_weights(2);
    num_nodes = order+1;
    
    x = basis_weights(3:elements*num_nodes+2);
    basis_weights = basis_weights(elements*num_nodes+3:end);
    elem_weights = zeros(elements, num_nodes);
    for i = 0:elements-1
        elem_weights(i+1, :) = basis_weights(i*num_nodes + 1:i*num_nodes + num_nodes);
    end
    
 
    figure
    hold on
    for i = 1:elements
        plot(x((i-1)*num_nodes+1:(i-1)*num_nodes + num_nodes), elem_weights(i, :), 'linewidth', 2);
    end
    axis([x(1) x(end) -inf inf])
    title("Initial Basis weights")
    
    % Analytic solution
    IC_file = "analytic_initial_condition.txt";
    file = fopen(IC_file);
    analytic_IC = fscanf(file, '%f');
    plot(x, analytic_IC, '--k','linewidth', 1.5)

end