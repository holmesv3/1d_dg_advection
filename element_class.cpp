#include "element_class.h"



ELEMENT::ELEMENT(int grid_solution_order, double left_node, double right_node) {

	// Initialize member variables
	element_solution_order = grid_solution_order;
	number_of_basis = grid_solution_order + 1;
	number_of_nodes = number_of_basis;

	left_bound = left_node;
	right_bound = right_node;

	left_value = 0; 
	right_value = 0;
	
	NEIGHBOR LEFT_SIDE();
	NEIGHBOR RIGHT_SIDE();

	c = 1; // Speed

	// Get the legendre weights and coordinates
	get_lgl_data(element_solution_order, lgl_weights, node_coordinates, left_bound, right_bound);

	// Initialize lagrange polynomials for the element
	for (int i = 0; i < number_of_basis; i++)
		element_lagrange_polynomials.push_back(lagrange_poly(i, node_coordinates));

	// Node value, flux vector, and stencil vector initialization
	dbl_vec temp;
	for (int i = 0; i < number_of_basis; i++) {
		temp.push_back(0);
		node_values.push_back(0);
		flux_vec.push_back(0);
		stencil_mass.push_back(0);
		stencil_stiff_flux.push_back(0);
	}
	// Mass, stiffness, and mass-inverse matrix initialization
	for (int i = 0; i < number_of_basis; i++) {
		mass_matrix.push_back(temp);
		mass_matrix_inverse.push_back(temp);
		stiffness_matrix.push_back(temp);
	}
	temp.clear();

	compute_mass_matrix(left_bound, right_bound, mass_matrix, element_lagrange_polynomials, lgl_weights);
	matrix_inverse(mass_matrix, mass_matrix_inverse);
	compute_stiffness_matrix(left_bound, right_bound, stiffness_matrix, element_lagrange_polynomials);
	
}
void ELEMENT::output_function_values(int m, double n, std::ofstream& handle) {
	for (double i = 0; i < n; i ++)
		handle << element_lagrange_polynomials[m](left_bound + (right_bound - left_bound)*i/(n-1)) << '\n';
}
void ELEMENT::print_element_data() {

	std::cout << "Mass matrix" << std::endl;
	for (int i = 0; i < number_of_basis; i++) {
		for (int j = 0; j < number_of_basis; j++)
			std::cout << mass_matrix[i][j] << '\t';
		std::cout << '\n';
	}

	std::cout << "Mass matrix inverse" << std::endl;
	for (int i = 0; i < number_of_basis; i++) {
		for (int j = 0; j < number_of_basis; j++)
			std::cout << mass_matrix_inverse[i][j] << '\t';
		std::cout << '\n';
	}


	std::cout << "Stiffness matrix" << std::endl;
	for (int i = 0; i < number_of_basis; i++) {
		for (int j = 0; j < number_of_basis; j++)
			std::cout << stiffness_matrix[i][j] << '\t';
		std::cout << '\n';
	}

	std::cout << "Flux vector" << std::endl;
	for (int i = 0; i < flux_vec.size(); i++) {
		std::cout << flux_vec[i] << '\t';
	std::cout << '\n';
	}
}

void ELEMENT::print_node_coordinates() {
	for (auto i = node_coordinates.begin(); i != node_coordinates.end(); i++) {
		std::cout << *i << '\t';
	}
	std::cout << '\n';
}
void ELEMENT::print_node_coordinates(std::ofstream& handle) {
	for (auto i = node_coordinates.begin(); i != node_coordinates.end(); i++) 
		handle << *i << '\n';
}

void ELEMENT::print_node_values() {
	for (int i = 0; i < number_of_nodes; i++)
		std::cout << node_values[i] << '\n';
}

void ELEMENT::print_node_values(std::ofstream& handle) {
	for (int i = 0; i < number_of_nodes; i++)
		handle << node_values[i] << '\n';
}

void ELEMENT::compute_stencil() {
	double k; // Stiffness term
	double m; // Mass term
	for (int i = 0; i < number_of_nodes; i++) {
		k = 0;
		m = 0;
		for (int j = 0; j < number_of_nodes; j++) {
			k += stiffness_matrix[j][i];
			m += mass_matrix_inverse[j][i];
		}
		stencil_mass[i] = m;
		stencil_stiff_flux[i] = c*k - flux_vec[i];
	}
}

void ELEMENT::get_initial_condition(IC_func& f) {
	compute_stencil();
	// Temporary computation variables
	func_vec temp_func_vec;
	dbl_vec temp_dbl_vec;
	func temp = std::bind(f, std::placeholders::_1, 0); // f(x, 0)  =f_0;

	// This takes care of integrating the RHS
	for (int i = 0; i < number_of_basis; i++) {
		temp_func_vec.push_back(std::bind(func_mult, std::placeholders::_1, temp, element_lagrange_polynomials[i]));
		temp_dbl_vec.push_back(gauss_lobatto(temp_func_vec[i], left_bound, right_bound, number_of_nodes));
	}

	// We already have the inverted mass matrix, 
	// so we only need to solve the vector-scalar 
	// products to find the initial basis weights 

	
	for (int i = 0; i < number_of_nodes; i++)
		node_values[i] = (right_bound - left_bound)*scalar_vector_multiplication(temp_dbl_vec[i], mass_matrix_inverse[i]);

	left_value = node_values.front();
	right_value = node_values.back();

}

void ELEMENT::perform_time_step(double dt) {
	node_values[0] += dt * stencil_mass[0]*(node_values[0]*stencil_stiff_flux[0] - *LEFT_SIDE.value*(LEFT_SIDE.flux) + node_values[1]*stencil_stiff_flux[1]);
	for (int i = 1; i < number_of_nodes-1; i++)
		node_values[i] += dt*stencil_mass[i]*(node_values[i]*stencil_stiff_flux[i] + node_values[i-1]*stencil_stiff_flux[i-1] + node_values[i+1]*stencil_stiff_flux[i+1]);
	node_values.back() += dt * stencil_mass.back() * (node_values.back() *stencil_stiff_flux.back() + node_values[number_of_nodes - 2] * (stencil_stiff_flux[number_of_nodes - 2] ) - *RIGHT_SIDE.value * RIGHT_SIDE.flux);
}

void ELEMENT::update_boundaries() {
	left_value = node_values.front();
	right_value = node_values.back();
}


void ELEMENT::analytic_solution(double t, IC_func& f, std::ofstream& handle) {
	for (int i = 0; i < number_of_nodes; i++) 
		handle << f(node_coordinates[i], t) << '\n';
}