#include "numerical_flux.h"

void upwind_flux(elem_vec elems) {
	for (int i = 0; i < elems.size(); i++) {
		elems[i]->flux_vec.front() = 0;
		elems[i]->flux_vec.back() =  1;
	}
}

void downwind_flux(elem_vec elems) {
	for (int i = 0; i < elems.size(); i++) {
		elems[i]->flux_vec.front() = 1;
		elems[i]->flux_vec.back() = 0;
	}
}